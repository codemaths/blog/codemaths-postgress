-- Deploy fresh database tables (orders matter)

\i '/docker-entrypoint-initdb.d/tables/posts_table.sql'
\i '/docker-entrypoint-initdb.d/tables/subscribers_table.sql'
